﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Net;
using System.Linq;
using System.Net;
using System.Globalization;

namespace ProbeScan
{
    class Program
    {
        private static string _url = "";
        private static string _deviceID = getDeviceID();
        static void Main(string[] args)
        {
            System.Threading.Thread.Sleep(20000);
            _url = System.Configuration.ConfigurationManager.AppSettings["URL"];

            //            ProcessLine("aa:bb:cc:dd:ee:ff -47");
            //ProcessLine("74:e5:43:68:d8:a3      -23     SwintonGuest");
            //ProcessLine("c4:85:08:c5:92:61      -51");
            //return;
            //return;           
            Process.Start(@"airmon-ng", "start wlan0").WaitForExit();
            System.Threading.Thread.Sleep(10000);

            var tshark = new Process();
            tshark.StartInfo.FileName = @"tshark";
            tshark.StartInfo.Arguments = @"-l -I -i mon0 -T fields -e wlan.sa -e radiotap.dbm_antsignal -e wlan_mgt.ssid subtype probe-req";
            //sudo tshark -I -i mon0 -T fields -e wlan.sa -e radiotap.dbm_antsignal -e wlan_mgt.ssid subtype probe-req

            //tshark.StartInfo.Arguments = string.Format(" -i " + _interfaceNumber + " -c " + int.MaxValue + " -w " + _pcapPath);
            //tshark.OutputDataReceived += new DataReceivedEventHandler(tshark_OutputDataReceived);
            //
            tshark.StartInfo.RedirectStandardOutput = true;
            tshark.StartInfo.UseShellExecute = false;
            tshark.StartInfo.CreateNoWindow = true;
            tshark.Start();

            Console.WriteLine("Device ID: " + getDeviceID());
            Console.WriteLine("Started Scanning, press any key to stop");

            while (!tshark.StandardOutput.EndOfStream)
            {
                string line = tshark.StandardOutput.ReadLine();

                System.Threading.ThreadPool.QueueUserWorkItem((o) => { ProcessLine(line); }); ;
            }

            // tshark.BeginOutputReadLine();
            while (Console.ReadKey() == null)
            {
            }

            tshark.Kill();
            Process.Start(@"airmon-ng", @"stop mon0").WaitForExit();
        }
        private static void tshark_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            ProcessLine(e.Data);
        }
        private static void ProcessLine(string line)
        {
            try
            {
                if (string.IsNullOrEmpty(line))
                    return;
                Console.WriteLine("Processing " + line);
                line = Regex.Replace(line, @"\s+", " ");
                if (!Regex.IsMatch(line, @"^??[:]??[:]??[:]??[:]??[:]??"))
                {
                    Console.WriteLine("Ignoring: " + line);
                    return;
                }
                Console.WriteLine("Sending: " + line);
                string[] parts = line.Split(' ');

                var payload = new System.Text.StringBuilder();
                payload.Append("{\"timestamp\": ");
                WriteDateTime(payload, DateTime.Now);
                //payload.AppendFormat("{{\"timestamp\": \"{0}\"", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                payload.AppendFormat(",\"deviceid\": \"{0}\"", _deviceID);
                payload.AppendFormat(",\"mac\": \"{0}\"", parts[0].Trim());
                payload.AppendFormat(",\"signalstrength\": \"{0}\"", parts[1].Trim());
                if (parts.Length > 2 && !parts[2].Contains("\\"))
                    payload.AppendFormat(",\"ssid\": \"{0}\"", parts[2].Trim());
                payload.Append("}");
                using (var client = new WebClient())
                {
                    client.Headers.Add("content-type", "application/json");//set your header here, you can add multiple headers
                    var response = client.UploadString(_url, payload.ToString());
                    //client.UploadString(_url, payload.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
                //throw;
            }
        }
        private static string getDeviceID()
        {
            string ipAddress = "";
            if (Dns.GetHostAddresses(Dns.GetHostName()).Any(ip => !IPAddress.IsLoopback(ip)))
            {
                ipAddress = Dns.GetHostAddresses(Dns.GetHostName()).First(ip => !IPAddress.IsLoopback(ip)).ToString();
            }
            return ipAddress;
        }

        private static void WriteDateTime(System.Text.StringBuilder output, DateTime dateTime)
        {
            // datetime format standard : yyyy-MM-dd HH:mm:ss
            DateTime dt = dateTime;
            
            output.Append('\"');
            output.Append(dt.Year.ToString("0000", NumberFormatInfo.InvariantInfo));
            output.Append('-');
            output.Append(dt.Month.ToString("00", NumberFormatInfo.InvariantInfo));
            output.Append('-');
            output.Append(dt.Day.ToString("00", NumberFormatInfo.InvariantInfo));
            output.Append('T'); // strict ISO date compliance 
            output.Append(dt.Hour.ToString("00", NumberFormatInfo.InvariantInfo));
            output.Append(':');
            output.Append(dt.Minute.ToString("00", NumberFormatInfo.InvariantInfo));
            output.Append(':');
            output.Append(dt.Second.ToString("00", NumberFormatInfo.InvariantInfo));
            //if (_params.DateTimeMilliseconds)
            //{
            //    _output.Append('.');
            //    _output.Append(dt.Millisecond.ToString("000", NumberFormatInfo.InvariantInfo));
            //}
            //if (_params.UseUTCDateTime)
            //    _output.Append('Z');

            output.Append('\"');
        }

    }
}
